﻿using UnityEngine;
using System.Collections;

namespace UI 
{
	public class MapPreferences {

		// public enum Difficulty {E, N, H};
		// public enum MapSize {S, M, L};

		public int difficulty { get; set; }
		public int mapSize { get; set; }
		
	}
}