﻿using UnityEngine;
using System.Collections;

namespace UI 
{
	public class SoundPreferences {

		public int musicVolume { get; set; }
		public int sfxVolume { get; set; }

	}
}