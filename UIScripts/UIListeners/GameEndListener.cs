﻿using UnityEngine;
using System.Collections;

namespace UI 
{

	public class GameEndListener : ILevelStatistics {

		[SerializeField]
		public WinningMenuController winningMenuController { set; get; }
		
		[SerializeField]
		public TimerController timerController { set; get; }

		public void OnLevelStatisticsListener(int playerScore) {
			winningMenuController.setPlayerScore(playerScore, timerController.timeString);
		}

	}
}