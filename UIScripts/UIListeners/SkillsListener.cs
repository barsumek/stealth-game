﻿using UnityEngine;
using System.Collections;

namespace UI 
{
	public class SkillsListener : ISkillStatusListener {

		[SerializeField]
		public SkillsPanelController controller { set; get; }

		public void SkillsLoaded(SkillType[] forTypes) {
			controller.loadSkills();
		}

		public void SkillStatusChanged(SkillType type) {
			controller.updateSkills();
		}

		public void SelectedSkillChanged(SkillType selected) {
			controller.updateSkills();
		}

	}
}