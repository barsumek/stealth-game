﻿using UnityEngine;
using System.Collections;

namespace UI 
{
	public class SceneListener {

		public void changeScene(UIManager.SceneTypes scene) {
			UIManager.SetScene(scene);
		}

		// manager mock function
		public void quit() {
			#if UNITY_EDITOR
	        	UnityEditor.EditorApplication.isPlaying = false;
			#else
	        	Application.Quit();
			#endif
		}

	}
}
