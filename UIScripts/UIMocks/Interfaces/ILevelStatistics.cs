﻿using UnityEngine;
using System.Collections;

namespace UI {
	
	public interface ILevelStatistics {

		void OnLevelStatisticsListener(int playerScore);
	}
}
