﻿using UnityEngine;
using System.Collections;

namespace UI 
{

	public interface ILevelsManager {

		void LoadNextLevel();
	    void RestartLevel();
	}
}