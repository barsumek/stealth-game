﻿using UnityEngine;
using System.Collections;

namespace UI 
{
	public interface IAudioManager {

		void ModifySoundPreferences(SoundPreferences soundPreferences);

	}
}