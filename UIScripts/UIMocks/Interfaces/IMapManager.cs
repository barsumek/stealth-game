using UnityEngine;
using System.Collections;

namespace UI {
	public interface IMapManager {

		void ModifyMapPreferences(MapPreferences mapPreferences);
		
	}

}