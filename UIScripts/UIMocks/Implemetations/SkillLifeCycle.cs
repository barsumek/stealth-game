﻿using UnityEngine;
using System.Collections;

namespace UI
{
	public enum SkillLifeCycle
	{
		Avaliable,
		Active,
		OnCooldown,
		Unavaliable
	}
}
