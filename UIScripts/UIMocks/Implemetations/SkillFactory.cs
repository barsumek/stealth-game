﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace UI {

	public class SkillFactory {

		public SkillType activeSkill { get; set; }

		public static SkillFactory getInstance() {
			return new SkillFactory();
		}

		public SkillType[] GetSkillTypeList() {

	        List <SkillType> avaliableSkills = new List <SkillType>();
	        avaliableSkills.Add(SkillType.Arrow);
	        avaliableSkills.Add(SkillType.TripleArrow);
	        avaliableSkills.Add(SkillType.SmokeBomb);
	        return avaliableSkills.ToArray(); 
		}

		public void AddStatusListener(ISkillStatusListener skillsListener) {

		}

		public SkillStatus GetSkillTypeStatus(SkillType type) {
			SkillStatus status = new SkillStatus();
			if(type == activeSkill) {
				status.LifeStatus = SkillLifeCycle.Active;
			}
			else status.LifeStatus = SkillLifeCycle.Avaliable;
			return status;
		}

		public SkillInfo GetSkillTypeInfo(SkillType type) {
			return new SkillInfo();
		}

	}
}
