﻿using UnityEngine;
using System.Collections;

namespace UI
{
	public class SkillStatus
	{
		public SkillLifeCycle LifeStatus { get; set; }
        public int HowMuchLeft { get; set; }

	}
}
