﻿using UnityEngine;
using System.Collections;

namespace UI 
{
	public class LevelsManager : MonoBehaviour, ILevelsManager {

		public void LoadNextLevel() {
			Debug.Log("next level loaded");
		}

	    public void RestartLevel() {
	    	Debug.Log("level restarted");
	    }

	}
}