﻿using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static void SetScene(SceneTypes scene)
    {
        Debug.Log("scene changed");
    }
      
    public void SetPause(bool pause) {
        Debug.Log("scene paused/unpaused");
    }

    public enum SceneTypes 
    {
        NotSet,
        MainMenu,
        Options,
        PrepareGame,
        Game,
        Lost,
        Win,
        Count
    }
}