﻿using UnityEngine;
using NUnit.Framework;
using UnityEngine.UI;

namespace UI 
{
    [TestFixture]
    public class GameEndListenerTest
    {

    	[Test]
    	public void GameEndedWithScore()
    	{
    		//Arrange
    		WinningMenuController winningMenuController = new GameObject().AddComponent<WinningMenuController>();
    			
    		UnityEngine.UI.Text playerScore = new GameObject().AddComponent<UnityEngine.UI.Text>();
            UnityEngine.UI.Text playerTime = new GameObject().AddComponent<UnityEngine.UI.Text>();           

            winningMenuController.playerScore = playerScore;
            winningMenuController.playerTime = playerTime;

            TimerController timerController = new GameObject().AddComponent<TimerController>();
            timerController.timeString = "00:01:37";

            //Act
            GameEndListener listener = new GameEndListener();
            listener.winningMenuController = winningMenuController;
            listener.timerController = timerController;
            listener.OnLevelStatisticsListener(3);
     			
    		//Assert
    		string text = "STARS: 3";
    		Assert.That(text == winningMenuController.playerScore.text);
    	}

    	[Test]
    	public void GameEndedWithTime()
    	{
            var timeString = "00:01:37";
            //Arrange
            WinningMenuController winningMenuController = new GameObject().AddComponent<WinningMenuController>();

            UnityEngine.UI.Text playerScore = new GameObject().AddComponent<UnityEngine.UI.Text>();
            UnityEngine.UI.Text playerTime = new GameObject().AddComponent<UnityEngine.UI.Text>();

            winningMenuController.playerScore = playerScore;
            winningMenuController.playerTime = playerTime;

            TimerController timerController = new GameObject().AddComponent<TimerController>();
            timerController.timeString = timeString;

            //Act
            GameEndListener listener = new GameEndListener();
            listener.winningMenuController = winningMenuController;
            listener.timerController = timerController;
            listener.OnLevelStatisticsListener(3);

            //Assert
            string text = "TIME: " + timeString;
            Assert.That(text == winningMenuController.playerTime.text);
        }
    }
}