﻿using UnityEngine;
using NUnit.Framework;
using UnityEngine.UI;

namespace UI 
{
    [TestFixture]
    public class MapPreferencesTest {


        private Slider setUpSlider(bool wholeNumbers, int minValue, int maxValue) {

            Slider slider = new GameObject().AddComponent<Slider>();
            slider.wholeNumbers = wholeNumbers;
            slider.minValue = minValue;
            slider.maxValue = maxValue;
            return slider;
        }

        private MapPreferencesController setUpMapPreferencesController
            (Slider difficulty, Slider mapSize, MapPreferences mp, int valueDifficulty, int valueMapSize) {

            MapPreferencesController controller = new GameObject().AddComponent<MapPreferencesController>();

            controller.difficultySlider = difficulty;
            controller.mapSizeSlider = mapSize;
            controller.mapPreferences = mp;

            difficulty.value = valueDifficulty;
            mapSize.value = valueMapSize;

            return controller;
        }

    	[Test]
    	public void DifficultyLevelChanged() {
    			
    		//Arrange
    		var difficultyLevel = 3;

    		MapPreferencesController controller = 
                setUpMapPreferencesController(setUpSlider(true, 1, 3), setUpSlider(true, 1, 3), new MapPreferences(), difficultyLevel, 3);

            //Act
            MapPreferences mapPreferences = controller.getMapPreferences();
    			
    		//Assert
    		Assert.That(difficultyLevel == mapPreferences.difficulty);		
    	}

        [Test]
        public void MapSizeLevelChanged() {

            //Arrange
            var mapSizeLevel = 2;

            MapPreferencesController controller = 
                setUpMapPreferencesController(setUpSlider(true, 1, 3), setUpSlider(true, 1, 3), new MapPreferences(), 3, mapSizeLevel);

            //Act
            MapPreferences mapPreferences = controller.getMapPreferences();
                
            //Assert
            Assert.That(mapSizeLevel == mapPreferences.mapSize);
        }
    }
}
