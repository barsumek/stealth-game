﻿using UnityEngine;
using NUnit.Framework;
using UnityEngine.UI;

namespace UI 
{
    [TestFixture]
    public class SoundPreferencesTest {


        private Slider setUpSlider(bool wholeNumbers, int minValue, int maxValue) {

            Slider slider = new GameObject().AddComponent<Slider>();
            slider.wholeNumbers = wholeNumbers;
            slider.minValue = minValue;
            slider.maxValue = maxValue;
            return slider;
        }

        private SoundPreferencesController setUpSoundPreferencesController
            (Slider music, Slider sfx, SoundPreferences sp, int musicValue, int sfxValue) {
            
            SoundPreferencesController controller = new GameObject().AddComponent<SoundPreferencesController>();

            controller.musicVolumeSlider = music;
            controller.sfxVolumeSlider = sfx;
            controller.soundPreferences = sp;

            music.value = musicValue;
            sfx.value = sfxValue;

            return controller;
        }


    	[Test]
    	public void MusicVolumeChanged() {

            //Arrange
            var musicVolume = 40;
            SoundPreferencesController controller = 
                setUpSoundPreferencesController(setUpSlider(true, 0, 100), setUpSlider(true, 0, 100), new SoundPreferences(), musicVolume, 60);

            //Act
            SoundPreferences soundPreferences = controller.getSoundPreferences();
    			
    		//Assert
    		Assert.That(musicVolume == soundPreferences.musicVolume);
    	}

        [Test]
        public void SfxVolumeChanged() {

            //Arrange
            var sfxVolume = 60;
            SoundPreferencesController controller = 
                setUpSoundPreferencesController(setUpSlider(true, 0, 100), setUpSlider(true, 0, 100), new SoundPreferences(), 40, sfxVolume);

            //Act
            SoundPreferences soundPreferences = controller.getSoundPreferences();

            //Assert
            Assert.That(sfxVolume == soundPreferences.sfxVolume);
        }
    }
}