﻿using UnityEngine;
using NUnit.Framework;
using UnityEngine.UI;
using System.Collections.Generic;

namespace UI 
{
    [TestFixture]
    public class SkillsPanelTest
    {

        [Test]
        public void ShowSkillsPanel()
        {
            //Arrange
            SkillsPanelController controller = new GameObject().AddComponent<SkillsPanelController>();

            SkillsListener listener = new SkillsListener();
            listener.controller = controller;
            SkillFactory factory = SkillFactory.getInstance();

            //Act
            factory.activeSkill = SkillType.TripleArrow;

            //Assert
            Assert.That(factory.GetSkillTypeStatus(SkillType.TripleArrow).LifeStatus == SkillLifeCycle.Active);
        }

    }
}