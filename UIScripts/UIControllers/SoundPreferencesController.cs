﻿using UnityEngine;
using UnityEngine.UI;

namespace UI 
{
	public class SoundPreferencesController : MonoBehaviour {

		public SoundPreferences soundPreferences { get; set; }

		[SerializeField]
		public Slider musicVolumeSlider { get; set; }

		[SerializeField]
		public Slider sfxVolumeSlider { get; set; }

	    public SoundPreferences getSoundPreferences() {
	        soundPreferences = new SoundPreferences();
	        soundPreferences.musicVolume = (int)musicVolumeSlider.value;
	        soundPreferences.sfxVolume = (int)sfxVolumeSlider.value;
			return soundPreferences;
		}
	}
}