﻿using UnityEngine; 
using System.Collections; 
 
 namespace UI 
{
    public class MouseCursorController : MonoBehaviour  { 

        [SerializeField]
        private Texture2D cursorTexture; 
      
        private bool customCursorEnabled; 
     
        void Start() { 
            Invoke("SetCustomCursor", 0.0f); 
        } 
     
        void OnDisable()  { 
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto); 
            customCursorEnabled = false; 
        } 
     
        private void SetCustomCursor() { 
            Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.Auto); 
            Debug.Log("Custom cursor set"); 
            customCursorEnabled = true; 
        } 
    } 
}