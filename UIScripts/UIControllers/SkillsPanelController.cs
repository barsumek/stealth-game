﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace UI 
{
    public class SkillsPanelController : MonoBehaviour {

    	[SerializeField]
    	public GameObject skillPrefab { get; set; }

    	[SerializeField]
    	public GameObject skillsPanel { get; set; }

    	private SkillFactory factory;

    	private SkillType [] skillTypes;
    	private Dictionary <SkillType, GameObject> skillsMap;

        private ISkillStatusListener skillsListener;

        public void loadSkills()
        {
            factory = SkillFactory.getInstance();
            skillsListener =  new SkillsListener();
            factory.AddStatusListener(skillsListener);

            skillTypes = factory.GetSkillTypeList();
            skillsMap = new Dictionary<SkillType, GameObject>();

            foreach (SkillType type in skillTypes)
            {
                GameObject skill = (GameObject)Instantiate(skillPrefab, skillsPanel.transform);
                skill.name = type.ToString();
                skillsMap.Add(type, skill);
            }
        }

        public void updateSkills()
        {
            foreach (var entry in skillsMap)
            {
                Text text = entry.Value.GetComponent<Text>();
                text.text = factory.GetSkillTypeInfo(entry.Key).Name + " : " + 
                    factory.GetSkillTypeStatus(entry.Key).LifeStatus.ToString() + " : " +
                    factory.GetSkillTypeStatus(entry.Key).HowMuchLeft;
                if (factory.GetSkillTypeStatus(entry.Key).LifeStatus == SkillLifeCycle.Active) text.color = Color.red;
            }
        }
    }
}
