﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace UI {

	public class MapParametersMenuController : MonoBehaviour {

		[SerializeField]
		private Button startNewGameButton;

		[SerializeField]
		private MapPreferencesController mapPreferencesController;

		private SceneListener sceneListener;
		
		private IMapManager mapManager;

		private ILevelsManager levelsManager;

		void Awake() {

	 		sceneListener = new SceneListener();
			mapManager = (MapManager)FindObjectOfType(typeof(MapManager));
			levelsManager = (LevelsManager)FindObjectOfType(typeof(LevelsManager));
			
			MapPreferences mapPreferences = mapPreferencesController.getMapPreferences();

			startNewGameButton.onClick.AddListener(() => {
				mapManager.ModifyMapPreferences(mapPreferences);
				sceneListener.changeScene(UIManager.SceneTypes.Game);
				levelsManager.LoadNextLevel();
			}); 
			
		}
	}
}
