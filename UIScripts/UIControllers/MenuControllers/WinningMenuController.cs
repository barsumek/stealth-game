﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace UI 
{
	public class WinningMenuController : MonoBehaviour {

		[SerializeField]
		private Button nextLevelButton;

		[SerializeField]
		private Button tryAgainButton;

		[SerializeField]
		private Button backToMenuButton;

		[SerializeField]
		public Text playerScore { get; set; }

		[SerializeField]
		public Text playerTime { get; set; }

		private ILevelsManager levelsManager;

		private SceneListener sceneListener;

		void Awake() {

			levelsManager = (LevelsManager)FindObjectOfType(typeof(LevelsManager));
			sceneListener = new SceneListener();

			nextLevelButton.onClick.AddListener(() => levelsManager.LoadNextLevel());
			tryAgainButton.onClick.AddListener(() => levelsManager.RestartLevel());
			backToMenuButton.onClick.AddListener(() => sceneListener.changeScene(UIManager.SceneTypes.MainMenu));
		}

		public void setPlayerScore(int score, string timeString) {
			playerScore.text = "STARS: " + score;
			playerTime.text = "TIME: " + timeString;
		}
	}
}
