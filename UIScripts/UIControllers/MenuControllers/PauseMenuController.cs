﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace UI {

	public class PauseMenuController : MonoBehaviour {

		[SerializeField]
		private Button resumeButton;
		[SerializeField]
		private Button restartButton;
		[SerializeField]
		private Button backToMenuButton;

		private SceneListener sceneListener;

		private ILevelsManager levelsManager;

		private UIManager uiManager;


		void Awake() {

			sceneListener = new SceneListener();
			levelsManager = (LevelsManager)FindObjectOfType(typeof(LevelsManager));
			uiManager = (UIManager)FindObjectOfType(typeof(UIManager));

			resumeButton.onClick.AddListener(() => {
				sceneListener.changeScene(UIManager.SceneTypes.Game);
				uiManager.SetPause(false);
			});
			restartButton.onClick.AddListener(() => levelsManager.RestartLevel());
			backToMenuButton.onClick.AddListener(() => sceneListener.changeScene(UIManager.SceneTypes.MainMenu));
		}
	}
}
