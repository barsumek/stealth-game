﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace UI {

	public class MainMenuController : MonoBehaviour {

		[SerializeField]
		private Button playButton;
		[SerializeField]
		private Button optionsButton;
		[SerializeField]
		private Button quitButton;

		private SceneListener sceneListener;

		void Awake() {

			sceneListener = new SceneListener();
			
			playButton.onClick.AddListener(() => sceneListener.changeScene(UIManager.SceneTypes.PrepareGame));
			optionsButton.onClick.AddListener(() => sceneListener.changeScene(UIManager.SceneTypes.Options));
			quitButton.onClick.AddListener(() => sceneListener.quit());
		}

	}
}
