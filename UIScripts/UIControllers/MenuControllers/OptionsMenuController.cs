﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace UI {
	public class OptionsMenuController : MonoBehaviour {

		[SerializeField]
		private Button saveButton;
		
		[SerializeField]
		private Button cancelButton;

		[SerializeField]
		private SoundPreferencesController soundPreferencesController;

		private SceneListener sceneListener;
		
		private IAudioManager audioManager;
			
		void Start() {
	 
	 		sceneListener = new SceneListener();
			audioManager = (AudioManager)FindObjectOfType(typeof(AudioManager));
			
			SoundPreferences soundPreferences = soundPreferencesController.getSoundPreferences();
			
			saveButton.onClick.AddListener(() => audioManager.ModifySoundPreferences(soundPreferences));
			saveButton.onClick.AddListener(() => sceneListener.changeScene(UIManager.SceneTypes.MainMenu));
			cancelButton.onClick.AddListener(() => sceneListener.changeScene(UIManager.SceneTypes.MainMenu));
		}
	}
}