﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace UI{

	public class GameOverMenuController : MonoBehaviour {

		[SerializeField]
		private Button tryAgainButton;
		
		[SerializeField]
		private Button backToMenuButton;

		private ILevelsManager levelsManager;

		private SceneListener sceneListener;

		void Awake() {

			levelsManager = (LevelsManager)FindObjectOfType(typeof(LevelsManager));
			sceneListener = new SceneListener();

			tryAgainButton.onClick.AddListener(() => levelsManager.RestartLevel());
			backToMenuButton.onClick.AddListener(() => sceneListener.changeScene(UIManager.SceneTypes.MainMenu));
		}

	}
}