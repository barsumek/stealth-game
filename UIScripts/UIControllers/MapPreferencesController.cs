﻿using UnityEngine;
using UnityEngine.UI;

namespace UI 
{
	public class MapPreferencesController : MonoBehaviour {

		public MapPreferences mapPreferences { get; set; }

		[SerializeField]
		public Slider difficultySlider { get; set; }

		[SerializeField]
		public Slider mapSizeSlider { get; set; }
		
		public MapPreferences getMapPreferences() {
			mapPreferences = new MapPreferences();
			mapPreferences.difficulty = (int)difficultySlider.value;
			mapPreferences.mapSize = (int)mapSizeSlider.value;
			return mapPreferences;
		}
	}
}