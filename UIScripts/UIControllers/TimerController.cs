﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace UI 
{
    public class TimerController : MonoBehaviour {

    	public string timeString { get; set; }

    	private bool timerOn = false;

    	private float time = 0.0f;
    	
    	[SerializeField]
    	private Text timer;

        public void timerStop() {
        	timerOn = false;
        }

        public void timerStart() {
        	timerOn = true;
        }

        void Awake() {
    		timerOn = true;
    	}

    	void Update() {
    		if (timerOn == true) {
         		time += Time.deltaTime * 1000;
         	}
    	}

        void OnGUI() {
        	float minutes = Mathf.Floor(time / 60000);
        	float seconds = Mathf.Floor((time % 60000) / 1000);
        	float miliseconds = Mathf.Floor(time % 1000);
        	timeString = minutes.ToString("00") + ":" + seconds.ToString("00") + ":" + miliseconds.ToString("000");
        	timer.text = "Time: " + timeString;
        }

    }
}