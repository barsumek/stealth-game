﻿using Assets.Code.Managers.ProgressManager;
using NUnit.Framework;
using Assets.Code.Managers.ProgressManager.Implementations;
using Assets.Code.Managers.ProgressManager.Interfaces;

namespace Assets.TestCode.Editor
{
    [TestFixture]
    class ProgressManagerImplTest
    {
        [Test]
        public void TestCreateValidLevelStatistics()
        {
            // given
            ProgressManagerImpl progressManager = new ProgressManagerImpl(null, 1);
            // expect
            int levelStatistics = progressManager.FillLevelStatistics();
            Assert.That(levelStatistics == 1);

            // given
            progressManager = new ProgressManagerImpl(null, 0);
            // expect
            levelStatistics = progressManager.FillLevelStatistics();
            Assert.That(levelStatistics == 2);

            // given
            progressManager = new ProgressManagerImpl(null, 1, true);
            // expect
            levelStatistics = progressManager.FillLevelStatistics();
            Assert.That(levelStatistics == 2);

            // given
            progressManager = new ProgressManagerImpl(null, 0, true);
            // expect
            levelStatistics = progressManager.FillLevelStatistics();
            Assert.That(levelStatistics == 3);

        }

        [Test]
        public void EnemyDiedTest()
        {
            // given
            ProgressManagerImpl progressManager = new ProgressManagerImpl(null, 1);
            // expect
            Assert.That(progressManager.NumOfEnemies == 1);
            progressManager.EnemyDied();
            Assert.That(progressManager.NumOfEnemies == 0);
        }

        [Test]
        public void TreasureFoundTest()
        {
            // given
            ProgressManagerImpl progressManager = new ProgressManagerImpl(null, 1);
            // expect
            Assert.That(!progressManager.isTreasureFound);
            progressManager.TreasureFound();
            Assert.That(progressManager.isTreasureFound);
        }
    }
}
