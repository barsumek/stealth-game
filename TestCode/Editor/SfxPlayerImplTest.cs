﻿using System;
using Assets.Code.Managers.AudioManager.Implementations;
using NUnit.Framework;
using UnityEngine;

namespace Assets.TestCode.Editor
{
    [TestFixture]
    class SfxPlayerImplTest
    {
        [Test]
        public void TestModifyVolume()
        {
            var obj = new GameObject();
            var audioSource = obj.AddComponent<AudioSource>();
            var sfxPlayer = new SfxPlayerImpl(audioSource);
            const float newVolume = 0.88f;
            sfxPlayer.ModifyVolume(newVolume);
            Assert.That(Math.Abs(sfxPlayer.SfxAudioSource.volume - newVolume) < 0.1);
        }
    }
}