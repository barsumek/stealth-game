﻿using System;
using Assets.Code.Managers.AudioManager.Implementations;
using NUnit.Framework;
using UnityEngine;

namespace Assets.TestCode.Editor
{
    [TestFixture]
    class MusicPlayerImplTest
    {
        [Test]
        public void TestModifyVolume()
        {
            var obj = new GameObject();
            var audioSource = obj.AddComponent<AudioSource>();
            var musicPlayer = new MusicPlayerImpl(audioSource);
            const float newVolume = 0.88f;
            musicPlayer.ModifyVolume(newVolume);
            Assert.That(Math.Abs(musicPlayer.MusicSource.volume - newVolume) < 0.1);
        }
    }
}
