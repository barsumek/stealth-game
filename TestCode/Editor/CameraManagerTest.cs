﻿using System;
using Assets.Code.Managers.CameraManager.Implementations;
using NUnit.Framework;
using UnityEngine;

namespace Assets.TestCode.Editor
{
    [TestFixture]
    public class CameraManagerTest
    {
        [Test]
        public void TestCameraPositionOutsideCameraChunk()
        {
            var camera = new GameObject();
            camera.transform.localPosition = new Vector3(0, 0);
            var player = new GameObject();
            player.transform.localPosition = new Vector3(2*Chunk.SIZE, Chunk.SIZE);
            var cameraManager = new CameraManagerImpl(player.transform, camera.transform);
            var newCamPos = cameraManager.SetCameraPosition();
            var checkCameraPos =
                (Math.Abs(Vector2.Distance(newCamPos,
                    new Vector2(2*Chunk.SIZE + Chunk.SIZE*0.5f, Chunk.SIZE + Chunk.SIZE*0.5f))) <=
                 Mathf.Epsilon);
            Assert.That(checkCameraPos);
        }

        [Test]
        public void TestCameraPositionInsideCameraChunk()
        {
            var camera = new GameObject();
            camera.transform.localPosition = new Vector3(0, 0);
            var player = new GameObject();
            player.transform.localPosition = new Vector3(1.5f, 0.9f);
            var cameraManager = new CameraManagerImpl(player.transform, camera.transform);
            var newCamPos = cameraManager.SetCameraPosition();
            var checkCameraPos =
                (Math.Abs(Vector2.Distance(newCamPos, new Vector2(Chunk.SIZE*0.5f, Chunk.SIZE*0.5f))) <=
                 Mathf.Epsilon);
            Assert.That(checkCameraPos);
        }
    }
}