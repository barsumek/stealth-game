﻿using System;
using System.Collections.Generic;
using Assets.Code.Generator;
using Assets.Code.Managers.LevelsManager.Implementations;
using NSubstitute;
using NSubstitute.Core;
using NUnit.Framework;
using UnityEngine;

namespace Assets.TestCode.Editor
{
    [TestFixture]
    public class LevelsManagerTest
    {
        private List<Vector3> patrolPoints = new List<Vector3>()
        {
            new Vector3(0, 0, 0),
            new Vector3(0, 2, 0),
            new Vector3(2, 2, 0)
        };

        IPlayerFactory SetupPlayerFactory()
        {
            IPlayerFactory playerFactory = Substitute.For<IPlayerFactory>();
            playerFactory.Create().Returns(new GameObject());
            return playerFactory;
        }

        IEnemyFactory SetupEnemyFactory()
        {
            IEnemyFactory enemyFactory = Substitute.For<IEnemyFactory>();
            enemyFactory.Create(EnemyType.Static, null).Returns(new GameObject());
            enemyFactory.Create(EnemyType.Dynamic,
                patrolPoints).Returns(new GameObject());


            return enemyFactory;
        }

        IMapGenerator SetupMapGenerator()
        {
            IMapGenerator mapGenerator = Substitute.For<IMapGenerator>();
            mapGenerator.GenerateMap(Difficulty.Easy, MapSize.Small).Returns(new GeneratedMap()
            {
                PlayerSpawnPosition = new Vector3(5, 0, 0),
                EnemiesToSpawn = new List<EnemyPosition>()
                {
                    new EnemyPosition()
                    {
                        enemyType = EnemyType.Static,
                        PatrolPositions = null,
                        SpawnPosition = new Vector3(10, 10, 0)
                    }
                    ,
                    new EnemyPosition()
                    {
                        enemyType = EnemyType.Dynamic,
                        PatrolPositions = patrolPoints,
                        SpawnPosition = new Vector3(0, 0, 0)
                    }
                },
                Map = new GameObject(),
                VictoryPosition = new Vector3(15, 15, 0)
            });
            return mapGenerator;
        }

        [Test]
        public void EnemiesCountTest()
        {
            var go = new GameObject();
            var manager = go.AddComponent<LevelsManager>();
            IPlayerFactory playerFactory = SetupPlayerFactory();
            IEnemyFactory enemyFactory = SetupEnemyFactory();
            IMapGenerator mapGenerator = SetupMapGenerator();

            manager.Init(playerFactory, enemyFactory, mapGenerator);
            //Load level
            manager.LoadNextLevel();
            Assert.That(manager.enemies.Count == 2);
        }

        [Test]
        public void RestartLevelTest()
        {
            var go = new GameObject();
            var manager = go.AddComponent<LevelsManager>();
            IPlayerFactory playerFactory = SetupPlayerFactory();
            IEnemyFactory enemyFactory = SetupEnemyFactory();
            IMapGenerator mapGenerator = SetupMapGenerator();

            manager.Init(playerFactory, enemyFactory, mapGenerator);
            //Load level
            manager.LoadNextLevel();
            foreach (var enemy in manager.enemies.Keys)
            {
                enemy.transform.Translate(new Vector3(20, 20, 20));
            }
            manager.Player.transform.Translate(new Vector3(10, 4, 2));
            manager.RestartLevel();
            var checkPlayerPos =
                Vector3.Distance(manager.Player.transform.position, manager.CurrentMap.PlayerSpawnPosition) <
                Mathf.Epsilon;
            bool checkEnemiesPos = true;
            foreach (var enemy in manager.enemies.Keys)
            {
                if (Vector3.Distance(enemy.transform.position, manager.enemies[enemy]) > Mathf.Epsilon)
                {
                    checkEnemiesPos = false;
                    break;
                }
            }
            Assert.That(checkPlayerPos && checkEnemiesPos);
        }
    }
}