﻿using NUnit.Framework;
using Assets.Code.Generator;
using UnityEngine;

namespace Assets.Editor.GeneratorTests
{
    [TestFixture]
    public class ChunkTest
    {
        [Test]
        public void ShiftOnceTest()
        {
            GameObject gameObject = new GameObject();
            gameObject.AddComponent<Chunk>();
            Chunk chunk = gameObject.GetComponent<Chunk>();
            chunk.EntranceMask = 8;
            chunk.RotateToMask(4);
            Assert.AreEqual(chunk.EntranceMask, 4);
            Assert.AreEqual(chunk.gameObject.transform.rotation.eulerAngles.y, 90);
        }

        [Test]
        public void ShiftTwiceTest()
        {
            GameObject gameObject = new GameObject();
            gameObject.AddComponent<Chunk>();
            Chunk chunk = gameObject.GetComponent<Chunk>();
            chunk.EntranceMask = 8;
            chunk.RotateToMask(2);
            Assert.AreEqual(chunk.EntranceMask, 2);
            Assert.AreEqual(chunk.gameObject.transform.rotation.eulerAngles.y, 180);
        }

        [Test]
        public void ShiftThriceTest()
        {
            GameObject gameObject = new GameObject();
            gameObject.AddComponent<Chunk>();
            Chunk chunk = gameObject.GetComponent<Chunk>();
            chunk.EntranceMask = 8;
            chunk.RotateToMask(1);
            Assert.AreEqual(chunk.EntranceMask, 1);
            Assert.AreEqual(chunk.gameObject.transform.rotation.eulerAngles.y, 270);
        }

    }
}
