﻿using NUnit.Framework;
using Assets.Code.Generator;

namespace Assets.Editor.GeneratorTests
{
    [TestFixture]
    public class MazeGeneratorTest
    {
        [Test]
        public void SmallMazeGenerationTest()
        {
            int small = (int) MapSize.Small;
            MazeGenerator generator = new MazeGenerator(small, small);
            bool[,] maze = generator.GenerateMaze();
            Assert.AreEqual(maze.GetLength(0), small);
            Assert.AreEqual(maze.GetLength(1), small);
        }

        [Test]
        public void MediumMazeGenerationTest()
        {
            int medium = (int) MapSize.Medium;
            MazeGenerator generator = new MazeGenerator(medium, medium);
            bool[,] maze = generator.GenerateMaze();
            Assert.AreEqual(maze.GetLength(0), medium);
            Assert.AreEqual(maze.GetLength(1), medium);
        }

        [Test]
        public void LargeMazeGenerationTest()
        {
            int large = (int) MapSize.Large;
            MazeGenerator generator = new MazeGenerator(large, large);
            bool[,] maze = generator.GenerateMaze();
            Assert.AreEqual(maze.GetLength(0), large);
            Assert.AreEqual(maze.GetLength(1), large);
        }
    }
}
