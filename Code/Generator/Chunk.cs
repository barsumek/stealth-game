﻿using UnityEngine;
using System.Collections.Generic;

namespace Assets.Code.Generator
{
    public class Chunk : MonoBehaviour
    {
        public const int Size = 160;
        public Difficulty Difficulty;
        public int EntranceMask;
        public List<EnemyPosition> EnemyPositions;

        public void RotateToMask(int newMask)
        {
            int rotations = 0;
            while (EntranceMask != newMask)
            {
                EntranceMask = (EntranceMask >> 1) + ((EntranceMask % 2) * 8);
                rotations++;
            }
            gameObject.transform.Rotate(0, 90 * rotations, 0);
        }
    }
}