﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Code.Generator
{
    [Serializable]
    public class EnemyPosition
    {
        public EnemyType enemyType;
        public Vector3 SpawnPosition;
        public List<Vector3> PatrolPositions;
    }
}