﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Code.Generator
{
    public class GeneratedMap
    {
        public GeneratedMap()
        {
            
        }

        public GeneratedMap(GameObject map, Vector3 playerSpawnPosition,
            Vector3 victoryPosition, Vector3 treasurePosition, List<EnemyPosition> enemiesToSpawn)
        {
            Map = map;
            PlayerSpawnPosition = playerSpawnPosition;
            VictoryPosition = victoryPosition;
            TreasurePosition = treasurePosition;
            EnemiesToSpawn = enemiesToSpawn;
        }

        public GameObject Map { get; set; }

        public Vector3 PlayerSpawnPosition { get; set; }

        public Vector3 VictoryPosition { get; set; }

        public Vector3 TreasurePosition { get; set; }

        public List<EnemyPosition> EnemiesToSpawn { get; set; }
    }
}