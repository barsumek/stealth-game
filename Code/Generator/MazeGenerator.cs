﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Assets.Code.Utils;
using Random = System.Random;

namespace Assets.Code.Generator
{
    public class MazeGenerator
    {

        private const char HASH = '#';
        private const char DOT = '.';
        private const char COMMA = ',';
        private const char EXCL = '?';
        private readonly int height;
        /*
        each cell of the maze is one of the following:
        '#' is wall
        '.' is empty space
        ',' is exposed but undetermined
        '?' is unexposed and undetermined
        */
        private readonly char[,] maze;
        private readonly int width;
        private readonly List<Pair<int, int>> frontier;

        private readonly Random rand;

        public MazeGenerator(int width, int height)
        {
            this.width = width;
            this.height = height;

            frontier = new List<Pair<int, int>>();
            rand = new Random();

            maze = new char[height, width];
            for (var y = 0; y < height; y++)
                for (var x = 0; x < width; x++)
                    maze[y, x] = EXCL;
        }

        public bool[,] GenerateMaze()
        {
            // choose a original point at random and carve it out.
            var xChoice = rand.Next(0, width - 1);
            var yChoice = rand.Next(0, height - 1);
            Carve(yChoice, xChoice);

            /*
            parameter branchrate:
            zero is unbiased, positive will make branches more frequent, negative will cause long passages
            this controls the position in the list chosen: positive makes the start of the list more likely,
            negative makes the end of the list more likely

            large negative values make the original point obvious

            try values between -10, 10
            */
            var branchrate = 0;

            while (frontier.Count > 0)
            {
                var pos = (float) rand.NextDouble();
                pos = Mathf.Pow(pos, Mathf.Exp(-branchrate));
                var frontierChoice = frontier[(int) (pos*frontier.Count)];
                if (Check(frontierChoice.First, frontierChoice.Second))
                    Carve(frontierChoice.First, frontierChoice.Second);
                else
                    Harden(frontierChoice.First, frontierChoice.Second);
                frontier.Remove(frontierChoice);
            }

            for (var y = 0; y < height; y++)
                for (var x = 0; x < width; x++)
                    if (maze[y, x] == EXCL)
                        maze[y, x] = HASH;

            return ConvertMazeToBool();
        }

        private void Carve(int y, int x)
        {
            var extra = new List<Pair<int, int>>();
            maze[y, x] = DOT;
            if ((x > 0) && (maze[y, x - 1] == EXCL))
            {
                maze[y, x - 1] = COMMA;
                extra.Add(new Pair<int, int>(y, x - 1));
            }
            if ((x < width - 1) && (maze[y, x + 1] == EXCL))
            {
                maze[y, x + 1] = COMMA;
                extra.Add(new Pair<int, int>(y, x + 1));
            }
            if ((y > 0) && (maze[y - 1, x] == EXCL))
            {
                maze[y - 1, x] = COMMA;
                extra.Add(new Pair<int, int>(y - 1, x));
            }
            if ((y < height - 1) && (maze[y + 1, x] == EXCL))
            {
                maze[y + 1, x] = COMMA;
                extra.Add(new Pair<int, int>(y + 1, x));
            }
            var shuffled = extra.OrderBy(e => rand.Next()); //for now enough for shuffling
            frontier.AddRange(shuffled);
        }

        private void Harden(int y, int x)
        {
            maze[y, x] = HASH;
        }

        private bool Check(int y, int x)
        {
            var edgeState = 0;
            if ((x > 0) && (maze[y, x - 1] == DOT))
                edgeState++;
            if ((x < width - 1) && (maze[y, x + 1] == DOT))
                edgeState += 2;
            if ((y > 0) && (maze[y - 1, x] == DOT))
                edgeState += 4;
            if ((y < height - 1) && (maze[y + 1, x] == DOT))
                edgeState += 8;

            return CheckNoDiagonals(y, x, edgeState);
        }

        private bool CheckNoDiagonals(int y, int x, int edgeState)
        {
            if (edgeState == 1)
            {
                if (x < width - 1)
                {
                    if ((y > 0) && (maze[y - 1, x + 1] == DOT))
                        return false;
                    if ((y < height - 1) && (maze[y + 1, x + 1] == DOT))
                        return false;
                }
                return true;
            }
            if (edgeState == 2)
            {
                if (x > 0)
                {
                    if ((y > 0) && (maze[y - 1, x - 1] == DOT))
                        return false;
                    if ((y < height - 1) && (maze[y + 1, x - 1] == DOT))
                        return false;
                }
                return true;
            }
            if (edgeState == 4)
            {
                if (y < height - 1)
                {
                    if ((x > 0) && (maze[y + 1, x - 1] == DOT))
                        return false;
                    if ((x < width - 1) && (maze[y + 1, x + 1] == DOT))
                        return false;
                }
                return true;
            }
            if (edgeState == 8)
            {
                if (y > 0)
                {
                    if ((x > 0) && (maze[y - 1, x - 1] == DOT))
                        return false;
                    if ((x < width - 1) && (maze[y - 1, x + 1] == DOT))
                        return false;
                }
                return true;
            }
            return false;
        }

        private bool[,] ConvertMazeToBool()
        {
            var mazeResult = new bool[height, width];
            for (var y = 0; y < height; y++)
                for (var x = 0; x < width; x++)
                    mazeResult[y, x] = maze[y, x] != HASH;
            return mazeResult;
        }
    }
}