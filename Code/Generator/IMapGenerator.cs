﻿
namespace Assets.Code.Generator
{
    public interface IMapGenerator
    {
        GeneratedMap GenerateMap(Difficulty difficulty, MapSize mapSize);
    }
}
