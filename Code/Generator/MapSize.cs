﻿
namespace Assets.Code.Generator
{
    public enum MapSize
    {
        Small = 4,
        Medium = 5,
        Large = 6
    }
}
