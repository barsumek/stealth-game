﻿using System.Collections.Generic;
using Assets.Code.Utils;

namespace Assets.Code.Generator
{
    public class MazeSolver
    {
        public Pair<int, int> PlayerSpawnPosition { get; set; }
        public Pair<int, int> VictoryPosition { get; set; }
        public Pair<int, int> TreasurePosition { get; set; }

        private bool[,] maze;
        private int mapLength;

        public MazeSolver(bool[,] maze, int mapLength)
        {
            this.maze = maze;
            this.mapLength = mapLength;
            PlayerSpawnPosition = new Pair<int, int>(0, 0);
            VictoryPosition = new Pair<int, int>(0, 0);
            TreasurePosition = new Pair<int, int>(0, 0);
        }

        public void Solve()
        {
            for (int y = 0; y < mapLength; y++)
            {
                for (int x = 0; x < mapLength; x++)
                {
                    if (maze[y, x])
                    {
                        PlayerSpawnPosition = new Pair<int, int>(x, y);
                    }
                }
            }

            for (int y = mapLength - 1; y > 0; y--)
            {
                for (int x = mapLength - 1; x > 0; x--)
                {
                    if (maze[y, x])
                    {
                        VictoryPosition = new Pair<int, int>(x, y);
                    }
                }
            }

            for (int y = 1; y < mapLength - 1; y++)
            {
                for (int x = 1; x < mapLength - 1; x++)
                {
                    if (maze[y, x])
                    {
                        TreasurePosition = new Pair<int, int>(x, y);
                    }
                }
            }
        }
    }
}