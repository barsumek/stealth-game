﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.Utils;
using Random = System.Random;

namespace Assets.Code.Generator
{
    public class MapGenerator : ScriptableObject, IMapGenerator
    {
        public static Random rand = new Random();

        public GeneratedMap GenerateMap(Difficulty difficulty, MapSize mapSize)
        {
            int mapLength = (int) mapSize;
            bool[,] maze = GenerateBoolMaze(mapLength);
            List<Chunk> chunks = LoadChunksFromResources().Where(c => c.Difficulty <= difficulty).ToList();

            return CreateMapFromChunks(chunks, maze, mapLength, difficulty);
        }

        private bool[,] GenerateBoolMaze(int mapLength)
        {
            return new MazeGenerator(mapLength, mapLength).GenerateMaze();
        }

        private List<Chunk> LoadChunksFromResources()
        {
            return Resources.LoadAll<Chunk>("Chunks").ToList();
        }

        private GeneratedMap CreateMapFromChunks(List<Chunk> chunks, bool[,] maze, int mapLength, Difficulty difficulty)
        {
            GeneratedMap map = new GeneratedMap();

            MazeSolver solver = new MazeSolver(maze, mapLength);
            solver.Solve();
            Pair<int, int> playerChunk = solver.PlayerSpawnPosition;
            Pair<int, int> victoryChunk = solver.VictoryPosition;
            Pair<int, int> treasureChunk = solver.TreasurePosition; 

            GameObject mapParent = new GameObject("Map");
            mapParent.transform.localPosition = Vector3.zero;

            List<EnemyPosition> enemyPositions = new List<EnemyPosition>();

            for (int y = 0; y < mapLength; y++)
            {
                for (int x = 0; x < mapLength; x++)
                {
                    if (maze[y, x])
                    {
                        int chunkMask = CalculateChunkMask(y, x, maze, mapLength);
                        int[] availableMasks = AvailableMasks(chunkMask);

                        List<Chunk> filtered = chunks.Where(c => availableMasks.Contains(c.EntranceMask)).ToList();
                        Chunk randomChunk = filtered[rand.Next(filtered.Count)];

                        if (x == playerChunk.First && y == playerChunk.Second)
                        {
                            map.PlayerSpawnPosition = GetRandomChunkSpawnPosition(randomChunk.EnemyPositions);
                        }
                        else if (x == victoryChunk.First && y == victoryChunk.Second)
                        {
                            map.VictoryPosition = GetRandomChunkSpawnPosition(randomChunk.EnemyPositions);
                        }
                        else if (x == treasureChunk.First && y == treasureChunk.Second)
                        {
                            map.TreasurePosition = GetRandomChunkSpawnPosition(randomChunk.EnemyPositions);
                        }
                        else
                        {
                            List<EnemyPosition> randomEnemyPositions =
                                GetRandomEnemyPositions(randomChunk.EnemyPositions, difficulty);
                            enemyPositions.AddRange(randomEnemyPositions);
                        }

                        GameObject newChunk = Instantiate(randomChunk.gameObject, Vector3.zero, Quaternion.identity) as GameObject;
                        Transform newChunkTransform = newChunk.transform;
                        newChunkTransform.SetParent(mapParent.transform);
                        newChunkTransform.localPosition = new Vector3(x * Chunk.Size, 0, -y * Chunk.Size);
                        newChunkTransform.GetComponent<Chunk>().RotateToMask(chunkMask);
                    }
                }
            }
            map.Map = mapParent;
            map.EnemiesToSpawn = enemyPositions;

            return map;
        }

        private int CalculateChunkMask(int y, int x, bool[,] maze, int mapLength)
        {
            int chunkMask = 0;
            if (y > 0 && maze[y - 1, x])
                chunkMask += 8; // top
            if (x < mapLength - 1 && maze[y, x + 1])
                chunkMask += 4; // right
            if (y < mapLength - 1 && maze[y + 1, x])
                chunkMask += 2; // bottom
            if (x > 0 && maze[y, x - 1])
                chunkMask += 1; // left
            return chunkMask;
        }

        private int[] AvailableMasks(int entranceMask)
        {
            var masks = new HashSet<int>();
            var nextMask = entranceMask;
            masks.Add(nextMask);
            // three rotates
            for (var i = 1; i < 4; i++)
            {
                nextMask = (nextMask >> 1) + ((nextMask%2)*8);
                masks.Add(nextMask);
            }
            return masks.ToArray();
        }

        private List<EnemyPosition> GetRandomEnemyPositions(List<EnemyPosition> allPositions, Difficulty difficulty)
        {
            List<EnemyPosition> positions = new List<EnemyPosition>(allPositions);

            var enemyCount = (int) Mathf.Ceil((int) difficulty * 2 / 7f * positions.Count);

            var chosenEnemies = new List<EnemyPosition>();
            for (var i = 0; i < enemyCount; i++)
            {
                var nextEnemy = rand.Next(positions.Count);
                chosenEnemies.Add(positions[nextEnemy]);
                positions.RemoveAt(nextEnemy);
            }
            return chosenEnemies;
        }

        private Vector3 GetRandomChunkSpawnPosition(List<EnemyPosition> positions)
        {
            return positions[rand.Next(positions.Count)].SpawnPosition;
        }
    }
}
