﻿namespace Assets.Code.Managers.AudioManager.Intefaces
{
    interface IAudioManager
    {
        void ModifySoundPreferences(SoundPreferences soundPreferences);
    }
}