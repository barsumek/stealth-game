﻿using UnityEngine;

namespace Assets.Code.Managers.AudioManager.Intefaces
{
    interface IMusicPlayer
    {
        void PauseMusic();
        void RestartMusic();
        void ModifyVolume(float soundVolume);
        void Play();
    }
}