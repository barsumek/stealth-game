﻿using UnityEngine;

namespace Assets.Code.Managers.AudioManager.Intefaces
{
    public interface ISfxPlayer
    {
        void Play(string soundId);
        void ModifyVolume(float soundVolume);
    }
}
