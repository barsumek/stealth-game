﻿public class SoundPreferences {
    public int sfxVolume { get; set; }
    public int musicVolume { get; set; }
}
