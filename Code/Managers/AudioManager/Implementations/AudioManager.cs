﻿using Assets.Code.Managers.AudioManager.Implementations;
using Assets.Code.Managers.AudioManager.Intefaces;
using UnityEngine;

namespace Assets.Code.Managers
{
    class AudioManagerImpl : MonoBehaviour, IAudioManager
    {
        private ISfxPlayer _sfxPlayer;
        private IMusicPlayer _musicPlayer;
        private SoundPreferences _soundPreferences;
        private const int DefaultMusicVolume = 50;
        private const int DefaultSfxVolume = 50;

        public void Start()
        {
            _sfxPlayer = new SfxPlayerImpl(gameObject.AddComponent<AudioSource>());
            _musicPlayer = new MusicPlayerImpl(gameObject.AddComponent<AudioSource>());
            _soundPreferences = new SoundPreferences();
            SetupDefaultSoundPreferences();
        }

        public void ModifySoundPreferences(SoundPreferences soundPreferences)
        {
            _sfxPlayer.ModifyVolume(soundPreferences.sfxVolume);
            _musicPlayer.ModifyVolume(soundPreferences.sfxVolume);
        }

        private void SetupDefaultSoundPreferences()
        {
            _soundPreferences.musicVolume = DefaultMusicVolume;
            _soundPreferences.sfxVolume = DefaultSfxVolume;
        }
    }
}
