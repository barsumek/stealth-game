﻿using Assets.Code.Managers.AudioManager.Intefaces;
using UnityEngine;

namespace Assets.Code.Managers.AudioManager.Implementations
{
    public class SfxPlayerImpl : ISfxPlayer
    {
        private AudioSource _sfxAudioSource;
        private const string SoundsPath = "/Resources/Sounds/";

        public SfxPlayerImpl(AudioSource sfxAudioSource)
        {
            this._sfxAudioSource = sfxAudioSource;
        }

        public void Play(AudioClip soundClip)
        {
            SfxAudioSource.PlayOneShot(soundClip);
        }

        public void Play(string soundId)
        {
            var audioClip = Resources.Load(SoundsPath + soundId) as AudioClip;
            Play(audioClip);
        }

        public void ModifyVolume(float soundVolume)
        {
            SfxAudioSource.volume = soundVolume;
        }

        public AudioSource SfxAudioSource
        {
            get { return _sfxAudioSource; }
        }
    }
}
