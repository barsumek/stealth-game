﻿using Assets.Code.Managers.AudioManager.Intefaces;
using UnityEngine;

namespace Assets.Code.Managers.AudioManager.Implementations
{
    public class MusicPlayerImpl : IMusicPlayer
    {
        private readonly AudioSource _musicSource;
        private const string MusicPath = "/Resources/Music/theme.mp3";
        private AudioClip musicClip;

        public MusicPlayerImpl(AudioSource musicSource)
        {
            this._musicSource = musicSource;
            musicClip = Resources.Load(MusicPath) as AudioClip;
            MusicSource.clip = musicClip;
        }

        public void PauseMusic()
        {
            MusicSource.Pause();
        }

        public void RestartMusic()
        {
            MusicSource.Stop();
            MusicSource.Play();
        }

        public void ModifyVolume(float musicVolume)
        {
            MusicSource.volume = musicVolume;
        }

        public void Play()
        {
            MusicSource.Play();
        }

        public AudioSource MusicSource
        {
            get { return _musicSource; }
        }
    }
}
