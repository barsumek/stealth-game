﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Code.Managers.ProgressManager.Interfaces
{
    interface IPauseListener
    {
        bool IsPause();
        void SetPause(bool pause);
    }
}
