﻿using Assets.Code.Managers.ProgressManager.Interfaces;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Code.Managers
{
    public class UIManager : MonoBehaviour, IPauseListener
    {
        private bool _isInit = false;
        public static SceneTypes _currentScene = SceneTypes.NotSet;
        public static SceneTypes _nextScene;

        private SceneStates _currentState = SceneStates.Preload;

        delegate void UpdateDelegate();

        private UpdateDelegate[] _updateDelegates;
        private AsyncOperation _asyncLoadSceneTask;

        public bool IsInit
        {
            get { return _isInit; }
        }


        public static void SetScene(SceneTypes scene)
        {
            _nextScene = scene;
        }

        public void Init()
        {
            if (_isInit) return;
            _updateDelegates = new UpdateDelegate[(int) SceneStates.Count];
            _updateDelegates[(int) SceneStates.Preload] = PreloadUpdate;
            _updateDelegates[(int) SceneStates.Load] = LoadUpdate;
            _updateDelegates[(int) SceneStates.Running] = RunningUpdate;
            _updateDelegates[(int) SceneStates.Unload] = UnloadUpdate;
            SetScene(SceneTypes.MainMenu);
            _isInit = true;
        }

        private void PreloadUpdate()
        {
            _currentScene = _nextScene;
            _asyncLoadSceneTask = SceneManager.LoadSceneAsync((int) _currentScene, LoadSceneMode.Additive);
            _currentState = SceneStates.Load;
        }

        private void LoadUpdate()
        {
            if (_asyncLoadSceneTask.isDone)
            {
                _asyncLoadSceneTask = null;
                _currentState = SceneStates.Running;
            }
            else
            {
                Debug.Log(_asyncLoadSceneTask.progress);
            }
        }

        private void RunningUpdate()
        {
            if (_nextScene != _currentScene)
            {
                _currentState = SceneStates.Unload;
            }
        }

        private void UnloadUpdate()
        {
            SceneManager.UnloadScene((int) _currentScene);
            _currentState = SceneStates.Preload;
        }

        #region Unity

        void Awake()
        {
            Init();
        }


        void Update()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Alpha1))
                SetScene(SceneTypes.MainMenu);
            if (Input.GetKeyDown(KeyCode.Alpha2))
                SetScene(SceneTypes.Options);
            if (Input.GetKeyDown(KeyCode.Alpha3))
                SetScene(SceneTypes.PrepareGame);
#endif

            if (_updateDelegates[(int) _currentState] != null)
            {
                _updateDelegates[(int) _currentState]();
            }
        }

        #endregion

        public bool IsPause()
        {
            return Time.timeScale == 0;
        }

        public void SetPause(bool pause)
        {
            Time.timeScale = pause ? 0 : 1;
        }
    }
}

public enum SceneStates
{
    Preload,
    Load,
    Running,
    Unload,
    Count
}

public enum SceneTypes
{
    NotSet,
    MainMenu,
    Options,
    PrepareGame,
    Game,
    Lost,
    Win,
    Count
}