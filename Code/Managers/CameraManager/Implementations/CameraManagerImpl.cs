﻿using System;
using Assets.Code.Managers.CameraManager.Interfaces;
using DG.Tweening;
using UnityEngine;

namespace Assets.Code.Managers.CameraManager.Implementations
{
    public class CameraManagerImpl : ICameraManager
    {
        private Transform _playerTransform;
        private Transform _cameraTransform;

        public CameraManagerImpl(Transform playerTransform, Transform cameraTransform)
        {
            _playerTransform = playerTransform;
            _cameraTransform = cameraTransform;
        }


        private Vector2 GetCameraMoveDestination()
        {
            var xchunkId = (int) _playerTransform.localPosition.x/Chunk.SIZE;
            var ychunkId = (int) _playerTransform.localPosition.y/Chunk.SIZE;

            //Computing camera destination
            var destX = xchunkId*Chunk.SIZE + Chunk.SIZE*0.5f;
            var destY = ychunkId*Chunk.SIZE + Chunk.SIZE*0.5f;
            return new Vector2(destX, destY);
        }

        /// <summary>
        /// Sets the camera position based on player's position
        /// </summary>
        /// <returns>Newly set camera position</returns>
        public Vector2 SetCameraPosition()
        {
            var dest = GetCameraMoveDestination();
            //Check, if have to move the camera
            if (Math.Abs(Vector2.Distance(_cameraTransform.localPosition, dest)) >= Mathf.Epsilon)
            {
                _cameraTransform.DOLocalMove(dest, 1f);
                return dest;
            }
            return _cameraTransform.localPosition;
        }
    }
}