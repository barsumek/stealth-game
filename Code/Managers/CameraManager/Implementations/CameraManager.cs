﻿using System;
using Assets.Code.Managers;
using Assets.Code.Managers.CameraManager.Interfaces;
using DG.Tweening;
using UnityEngine;

namespace Assets.Code.Managers.CameraManager.Implementations
{
    public class CameraManager : MonoBehaviour
    {
        public Transform PlayerTransform { get; set; }
        public Transform CameraTransform { get; set; }

        private ICameraManager _cameraManager;

        void Awake()
        {
            //Create camera object
            var cameraGameObject = new GameObject("MainCamera");
            cameraGameObject.AddComponent<Camera>();
            cameraGameObject.tag = "MainCamera";
            cameraGameObject.AddComponent<GUILayer>();
            cameraGameObject.AddComponent<FlareLayer>();
            cameraGameObject.AddComponent<AudioListener>();
            CameraTransform = cameraGameObject.transform;
            CameraTransform.SetParent(transform.parent, false);

            var playerGameObject = new GameObject("Player");
            PlayerTransform = playerGameObject.transform;
            PlayerTransform.SetParent(transform.parent, false);

            _cameraManager = new CameraManagerImpl(PlayerTransform, CameraTransform);
        }

        void Update()
        {
            _cameraManager.SetCameraPosition();
        }
    }

    public class Chunk
    {
        public const int SIZE = 2;
    }
}