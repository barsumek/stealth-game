﻿using UnityEngine;

namespace Assets.Code.Managers.CameraManager.Interfaces
{
    public interface ICameraManager
    {
        Vector2 SetCameraPosition();
    }
}
