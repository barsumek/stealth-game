﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Assets.Code.Managers;
using Assets.Code.Managers.CameraManager.Implementations;
using Assets.Code.Managers.LevelsManager.Implementations;
using UnityEngine;

public class Initializer : MonoBehaviour
{
    private bool _isInit = false;

    public bool IsInit
    {
        get { return _isInit; }
    }


    public CameraManager CameraManager { get; private set; }
    public UIManager UiManager { get; private set; }

    public LevelsManager LevelsManager { get; private set; }
    private GameObject _managers;

    void InitManagers()
    {
        _managers = transform.GetChild(0).gameObject;
       
        CameraManager = _managers.AddComponent<CameraManager>();
        UiManager = _managers.AddComponent<UIManager>();
        LevelsManager = _managers.AddComponent<LevelsManager>();
        //Inject dependencies with Init()
    }

    void Awake()
    {
        Init();
    }

    private void Init()
    {
        InitManagers();
    }
}