﻿namespace Assets.Code.Managers.LevelsManager.Interfaces
{
    public interface ILevelsManager
    {
        void LoadNextLevel();
        void RestartLevel();
    }
}