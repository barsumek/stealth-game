﻿using System.Collections.Generic;
using Assets.Code.Generator;
using Assets.Code.Managers.LevelsManager.Interfaces;
using Assets.Code.Managers.ProgressManager.Implementations;
using Assets.Code.Managers.ProgressManager.Interfaces;
using Assets.Code.Managers.ProgressManager.Mocks;
using UnityEngine;

namespace Assets.Code.Managers.LevelsManager.Implementations
{
    public class LevelsManager : MonoBehaviour, ILevelsManager
    {
        private bool _isInit = false;
        private IMapGenerator _mapGenerator;
        private GeneratedMap _currentMap;
        public Difficulty Difficulty { get; set; }
        public MapSize MapSize { get; set; }

        private IEnemyFactory _enemyFactory;
        private IPlayerFactory _playerFactory;
        private IProgressManager _progressManager;
        private ILevelStatisticsListener statisticsListener;
        //Stores references to all enemies on current level and their starting positions
        public Dictionary<GameObject, Vector3> enemies { get; set; }
        private GameObject _player;


        public bool IsInit
        {
            get { return _isInit; }
        }

        public IProgressManager ProgressManager
        {
            get { return _progressManager; }
        }

        public GameObject Player
        {
            get { return _player; }
        }

        public GeneratedMap CurrentMap
        {
            get { return _currentMap; }
        }

        public void Init(IPlayerFactory playerFactory, IEnemyFactory enemyFactory, IMapGenerator mapGenerator)
        {
            if (_isInit) return;
            _playerFactory = playerFactory;
            _enemyFactory = enemyFactory;
            _mapGenerator = mapGenerator;
            enemies = new Dictionary<GameObject, Vector3>();
            statisticsListener = FindObjectOfType<GameEndListener>();
            _isInit = true;
        }

        public void LoadNextLevel()
        {
            //Delete current map
            if (CurrentMap != null)
            {
                ClearEnemies();
                Destroy(CurrentMap.Map);
            }

            //Create new map
            _currentMap = _mapGenerator.GenerateMap(Difficulty, MapSize);
            //Assign enemies
            foreach (var enemyProp in _currentMap.EnemiesToSpawn)
            {
                var enemy = _enemyFactory.Create(enemyProp.enemyType, enemyProp.PatrolPositions);
                enemy.transform.position = enemyProp.SpawnPosition;
                var dieComponent = enemy.GetComponent<DieComponent>();
                if (dieComponent != null)
                    dieComponent.OnDied += _progressManager.EnemyDied;

                enemies.Add(enemy, enemyProp.SpawnPosition);
            }

            //Assign player
            _player = _playerFactory.Create();
            _player.transform.position = _currentMap.PlayerSpawnPosition;
            _progressManager = new ProgressManagerImpl(statisticsListener, enemies.Count);
        }

        private void ClearEnemies()
        {
            foreach (var enemy in enemies.Keys)
            {
                enemy.GetComponent<DieComponent>().OnDied -= ProgressManager.EnemyDied;
            }
            enemies.Clear();
        }

        public void RestartLevel()
        {
            //Reset enemies positions
            foreach (var enemy in enemies.Keys)
            {
                enemy.transform.position = enemies[enemy];
            }
            Player.transform.position = CurrentMap.PlayerSpawnPosition;
            _progressManager = new ProgressManagerImpl(statisticsListener, enemies.Count);
        }
    }

    internal class GameEndListener : MonoBehaviour, ILevelStatisticsListener
    {
        public void OnLevelStatisticsListener(int playerScore)
        {
            throw new System.NotImplementedException();
        }
    }

    public interface IPlayerFactory
    {
        GameObject Create();
    }

    public class DieComponent : MonoBehaviour
    {
        public delegate void Die();

        public event Die OnDied;
    }

    public interface IEnemyFactory
    {
        GameObject Create(EnemyType type, List<Vector3> patrolPositions);
    }
}