﻿using Assets.Code.Managers.ProgressManager.Interfaces;
using Assets.Code.Managers.ProgressManager.Mocks;
using UnityEngine;

namespace Assets.Code.Managers.ProgressManager.Implementations
{
    public class ProgressManagerImpl : IProgressManager
    {
        private bool _treasureFound;
        private int _numOfEnemies;
        private ILevelStatisticsListener _levelStatisticsListener;

        public void EnemyDied()
        {
            _numOfEnemies--;
        }

        public void PlayerDied()
        {
            UIManager.SetScene(SceneTypes.Lost);
        }

        public void TreasureFound()
        {
            _treasureFound = true;
        }

        public void LevelGoalReached()
        {
            var stats = FillLevelStatistics();
            _levelStatisticsListener.OnLevelStatisticsListener(stats);
            UIManager.SetScene(SceneTypes.Win);
        }

        public ProgressManagerImpl(ILevelStatisticsListener levelStatisticsListener, int numOfEnemies,
            bool treasureFound = false)
        {
            _treasureFound = treasureFound;
            _numOfEnemies = numOfEnemies;
            _levelStatisticsListener = levelStatisticsListener;
        }

        public int FillLevelStatistics()
        {
            var result = 1;
            bool allEnemieskilled = (_numOfEnemies <= 0);
            result += (allEnemieskilled) ? 1 : 0;
            result += _treasureFound ? 1 : 0;
            return result;
        }

        public bool isTreasureFound
        {
            get { return _treasureFound; }
        }

        public int NumOfEnemies
        {
            get { return _numOfEnemies; }
        }
    }
}