﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Code.Managers.ProgressManager.Mocks
{
    public interface ILevelStatisticsListener
    {
        void OnLevelStatisticsListener(int playerScore);
    }
}
