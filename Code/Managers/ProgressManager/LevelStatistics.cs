﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Code.Managers.ProgressManager
{
    public class LevelStatistics
    {
        public bool _allEnemiesKilled { get; set; }
        public bool _treasureFound { get; set; }

        public LevelStatistics(bool allEnemiesKilled, bool treasureFound)
        {
            _allEnemiesKilled = allEnemiesKilled;
            _treasureFound = treasureFound;
        }
    }
}
