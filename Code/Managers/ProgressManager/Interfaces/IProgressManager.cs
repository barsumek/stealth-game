﻿namespace Assets.Code.Managers.ProgressManager.Interfaces
{
    public interface IProgressManager : IEnemyDiedListener, IPlayerDiedListener, ITreasureGoalListener
    {
     
    }
}
