﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Code.Managers.ProgressManager.Interfaces
{
    public interface ITreasureGoalListener
    {
        void LevelGoalReached();
        void TreasureFound();
    }
}
